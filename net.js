var serverpingreq = false,
    serverpingr = new XMLHttpRequest({
        mozSystem: true
    }),
    serverpingrfinish = new Event('ploaded'),
    currentsongdata = {
        'length': 0,
        'endtime': 1,
        'advert': false,
        'live': false,
        'albumid': '',
        'btmtobj': null
    };

function checkmetadatagrab() {
    if (document.visibilityState === 'visible' || prefm('read', 'bgload')) {
        if (Math.floor(new Date().getTime() / 1000) >= currentsongdata.endtime) {
            metadatagrab(0);
            eid('mdata-various-right').textContent = '--:--';
            eid('mdataprog').style.width = '100%'
        } else {
            if (currentsongdata.live) {
                eid('mdata-various-right').textContent = 'LIVE: ' + timeformat(
                    Math.abs(
                        currentsongdata.length - 
                        Math.floor(new Date().getTime() / 1000)
                    )
                );
                eid('mdataprog').style.width = '100%';
            } else {
                eid('mdata-various-right').textContent = timeformat(
                    (
                        currentsongdata.endtime - 
                        Math.floor(new Date().getTime() / 1000)
                    )
                );
                eid('mdataprog').style.width = ((currentsongdata.length - (currentsongdata.endtime - Math.floor(new Date().getTime() / 1000))) / currentsongdata.length) * 100 + '%'
            }
        }
    }
}

function metadatagrab(grabc) {
    if (!navigator.onLine) {
        alertMessage('Please check your internet connection...', 5000, 2)
    }
    if (!serverpingreq) {
        serverpingreq = true;
        switch (grabc) {
            case 0:
                var toping = 'https://gensokyoradio.net/api/station/playing/';
                serverpingr.onreadystatechange = metadatagrabdone;
                serverpingr.onerror = metadatagraberror;
                break;
            case 1:
                var toping = 'https://gensokyoradio.net/api/station/history/';
                serverpingr.onreadystatechange = historygrabdone;
                serverpingr.onerror = historygraberror;
                break
        }
        serverpingr.open('GET', toping);
        serverpingr.send()
    }
}

function metadatagraberror(e) {
    leftsongstatusupdate('mdataerr');
    console.log(e);
    setTimeout(metadatagrabfinalize, 5000)
}

function metadatagrabdone() {
    if (serverpingr.readyState === 4) {
        var resp = JSON.parse(serverpingr.response);
        switch (resp.SERVERINFO.MODE) {
            case 'ADVERT':
                currentsongdata.advert = true;
                currentsongdata.live = false;
                currentsongdata.endtime = resp.SONGTIMES.SONGEND;
                currentsongdata.length = resp.SONGTIMES.DURATION;
                if (Math.floor(new Date().getTime() / 1000) >= currentsongdata.endtime) {
                    currentsongdata.endtime = Math.floor(new Date().getTime() / 1000) + 10;
                    currentsongdata.length = Math.floor(new Date().getTime() / 1000) + 30
                }
                eid('mdata-title').textContent = 'Intermission';
                eid('mdata-artist').textContent = 'Wait warmly...';
                eid('mdata-album').textContent = 'We are resting for now.';
                eid('mdataimg').src = '/img/intermission_0.png';
                eid('mdata-imgbg').style.backgroundImage = null;
                metadatablanker();
                eid('moreinfo-title').textContent = 'Girls do their best now and are preparing. Please wait warmly until it is ready~';
                eid('moreinfo-tstart').textContent = dateformat(resp.SONGTIMES.SONGSTART);
                eid('moreinfo-tend').textContent = dateformat(resp.SONGTIMES.SONGEND);
                eid('moreinfo-tlength').textContent = timeformat(resp.SONGTIMES.DURATION);
                eid('mdata-various-listeners').textContent = resp.SERVERINFO.LISTENERS;
                currentsongdata.btmtobj = {
                    title: 'Intermission',
                    duration: resp.SONGTIMES.DURATION * 1000
                };
                break;
            case 'RADIO':
                currentsongdata.advert = false;
                currentsongdata.live = false;
                currentsongdata.endtime = resp.SONGTIMES.SONGEND;
                currentsongdata.length = resp.SONGTIMES.DURATION;
                currentsongdata.albumid = resp.SONGDATA.ALBUMID;
                eid('mdata-title').textContent = resp.SONGINFO.TITLE;
                eid('mdata-artist').textContent = resp.SONGINFO.ARTIST;
                eid('mdata-album').textContent = resp.SONGINFO.ALBUM;
                var imageurl;
                if (resp.MISC.ALBUMART === '' || !prefm('read', 'imgload')) {
                    eid('mdataimg').src = '/img/noalbum.png';
                    eid('mdata-imgbg').style.backgroundImage = null;
                } else {
                    imageurl = 'https://gensokyoradio.net/images/albums/500/' + resp.MISC.ALBUMART;
                    eid('mdataimg').src = imageurl;
                    eid('mdata-imgbg').style.backgroundImage = `url(${imageurl})`;
                    eid('mdataimgoverlay').style.opacity = 1
                }
                eid('moreinfo-title').textContent = resp.SONGINFO.TITLE;
                eid('moreinfo-artist').textContent = resp.SONGINFO.ARTIST;
                eid('moreinfo-album').textContent = resp.SONGINFO.ALBUM;
                eid('moreinfo-circle').textContent = resp.SONGINFO.CIRCLE;
                if (resp.MISC.CIRCLELINK === '') {
                    eid('moreinfo-circlesite').textContent = 'No website available'
                } else {
                    eid('moreinfo-circlesite').textContent = resp.MISC.CIRCLELINK
                }
                /* if (resp.MISC.CIRCLEART === '' || !prefm('read', 'imgload')) {
                    eid('moreinfo-circleimg').src = '/img/nocircle.png'
                } else {
                    eid('moreinfo-circleimg').src = 'https://gensokyoradio.net/images/circles/' + resp.MISC.CIRCLEART
                } */
                //if (resp.SONGINFO.YEAR === 0) {
                if (!resp.SONGINFO.YEAR) { //do you trust javascript falsy?
                    eid('moreinfo-year').textContent = '----'
                } else {
                    eid('moreinfo-year').textContent = resp.SONGINFO.YEAR
                }
                eid('moreinfo-sid').textContent = resp.SONGDATA.SONGID + '/' + resp.SONGDATA.ALBUMID;
                eid('mdata-various-listeners').textContent = resp.SERVERINFO.LISTENERS;
                eid('moreinfo-raters').textContent = resp.SONGDATA.TIMESRATED || 'nobody';
                var songRating = resp.SONGDATA.RATING;
                if(typeof(songRating) === 'string') {
                    if(songRating === 'N/A') {
                        songRating = 0;
                    } else {
                        songRating = songRating.split('/');
                        songRating = (songRating[0] / songRating[1]) * 5;
                    }
                } /* else {
                    songRating = parseFloat(songRating);
                } */
                eid('moreinfo-rating').textContent = songRating || 'No';
                for (var i = 0; i < 5; i += 1) {
                    var starwidth = Math.max(
                        (songRating - i) * 100,
                        0
                    );
                    document.getElementsByClassName('moreinfo-rating-star-in')[i].style.width = starwidth + '%'
                }
                eid('moreinfo-tstart').textContent = dateformat(resp.SONGTIMES.SONGSTART);
                eid('moreinfo-tend').textContent = dateformat(resp.SONGTIMES.SONGEND);
                eid('moreinfo-tlength').textContent = timeformat(resp.SONGTIMES.DURATION);
                currentsongdata.btmtobj = {
                    title: resp.SONGINFO.TITLE,
                    artist: resp.SONGINFO.ARTIST + ' / ' + resp.SONGINFO.CIRCLE,
                    album: resp.SONGINFO.ALBUM,
                    duration: resp.SONGTIMES.DURATION * 1000
                };
                switch (currentpage) {
                    case 2:
                    case 4:
                        alertMessage('Now playing: \r\n' + resp.SONGINFO.TITLE, 2500, 0, imageurl);
                        break;
                    case 3:
                        minforeset();
                        break
                }
                break;
            case 'EVENT':
                if (!currentsongdata.live) {
                    metadatablanker();
                    currentsongdata.advert = false;
                    currentsongdata.live = true;
                    currentsongdata.length = Math.floor(new Date().getTime() / 1000);
                    currentsongdata.endtime = Math.floor(new Date().getTime() / 1000) + (60 * 10);
                    currentsongdata.albumid = 0;
                    eid('mdata-title').textContent = resp.SONGINFO.TITLE;
                    eid('mdata-artist').textContent = resp.SONGINFO.ARTIST;
                    eid('mdata-album').textContent = resp.SONGINFO.ALBUM;
                    eid('moreinfo-title').textContent = resp.SONGINFO.TITLE;
                    eid('moreinfo-artist').textContent = resp.SONGINFO.ARTIST;
                    eid('moreinfo-album').textContent = resp.SONGINFO.ALBUM;
                    eid('moreinfo-circle').textContent = resp.SONGINFO.CIRCLE;
                    eid('moreinfo-circlesite').textContent = 'http://www.gensokyoradio.net/';
                    //eid('moreinfo-circleimg').src = '/img/nocircle.png';
                    if (resp.MISC.ALBUMART === '' || !prefm('read', 'imgload')) {
                        eid('mdataimg').src = '/img/live.png';
                        eid('mdata-imgbg').style.backgroundImage = null;
                    } else {
                        var albumart = 'https://gensokyoradio.net/images/albums/500/' + resp.MISC.ALBUMART;
                        eid('mdataimg').src = albumart;
                        eid('mdata-imgbg').style.backgroundImage = `url(${albumart})`
                        eid('mdataimgoverlay').style.opacity = 1
                    }
                    currentsongdata.btmtobj = {
                        title: resp.SONGINFO.TITLE,
                        artist: resp.SONGINFO.ARTIST + ' / ' + resp.SONGINFO.CIRCLE,
                        album: resp.SONGINFO.ALBUM,
                        duration: 60 * 60 * 1000
                    }
                } else {
                    currentsongdata.endtime += 60 * 10;
                }
                eid('mdata-various-listeners').textContent = resp.SERVERINFO.LISTENERS;
                break;
            default:
                break
        }
        eid('moreinfo-lastupdate').textContent = dateformat(Math.floor(new Date().getTime() / 1000));
        setTimeout(metadatagrabfinalize, 5000)
    }
}

function metadatagrabfinalize() {
    serverpingreq = false;
    serverpingr.dispatchEvent(serverpingrfinish)
}

function metadatablanker() {
    eid('moreinfo-title').textContent = '----';
    eid('moreinfo-artist').textContent = '----';
    eid('moreinfo-album').textContent = '----';
    eid('moreinfo-circle').textContent = '----';
    eid('moreinfo-circlesite').textContent = 'No website available';
    //eid('moreinfo-circleimg').src = '/img/nocircle.png';
    eid('moreinfo-year').textContent = '----';
    eid('moreinfo-sid').textContent = '---- / ----';
    eid('moreinfo-rating').textContent = '-.-';
    eid('moreinfo-raters').textContent = '--';
    eid('moreinfo-tstart').textContent = '....-..-.. --:--:--';
    eid('moreinfo-tend').textContent = '....-..-.. --:--:--';
    eid('moreinfo-tlength').textContent = '--:--';
    eid('mdata-various-listeners').textContent = '--';
    for (var i = 0; i < 5; i += 1) {
        document.getElementsByClassName('moreinfo-rating-star-in')[i].style.width = '0'
    }
}