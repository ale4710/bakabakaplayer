var moreinfosel = 0;

function moreinfoK(k) {

	if(currentpage !== 3) {
		currentpage = 3;
		infscreenshowing = true;
		eid('moreinfo').style.opacity = 1;
		document.body.classList.add('minfoshow');

		moreinfosel = 0;
		eid('moreinfo').scrollTo(0,0);
		if(document.getElementsByClassName('moreinfo-entry').length !== 0) {
			document.getElementsByClassName('moreinfo-entry')[0].focus();
		}

	} else {

		if(keyisnav(k)) {k.preventDefault();}

		switch(k.key) {
			case 'SoftLeft':
				document.body.classList.remove('minfoshow');
				eid('moreinfo').style.opacity = null;
				currentpage = 0;
				infscreenshowing = false;
				updatenavbar();
				break;

			case 'ArrowUp':
				minfscroll(false);
				break;

			case 'ArrowDown':
				minfscroll(true);
				break;

			case 'Enter':
				if(currentsongdata.advert || currentsongdata.live) {break;}
				switch(document.activeElement.children[0].id.substr(9)) { //9 = length of moreinfo-
					case 'album':
						openalbum(currentsongdata.albumid);
						break;
					case 'circle':
						if(eid('moreinfo-circlesite').textContent !== 'No website available') {
							openlink(eid('moreinfo-circlesite').textContent);
						}
						break;
				}
				break;


		}

	}
}

function navbaropendispcheck() {
	if(currentsongdata.advert || currentsongdata.live) {return false;}
	switch(document.activeElement.children[0].id.substr(9)) { //9 = length of moreinfo-
		case 'album':
			return true;
		case 'circle':
			if(eid('moreinfo-circlesite').textContent !== 'No website available') {
				return true;
			}
			break;
	}
	return false;
}

function minforeset() {
	eid('moreinfo').scrollTo(0,0);
	document.getElementsByClassName('moreinfo-entry')[0].focus();
	moreinfosel = 0;
	updatenavbar();
}

function minfscroll(dn) {
	if(dn) {
		var num = 1;
	} else {
		var num = -1;
	}
	moreinfosel = navigatelist(moreinfosel,document.getElementsByClassName('moreinfo-entry'),num);
	scrolliv(document.activeElement,dn,eid('moreinfo'));
	if(moreinfosel === 0) {eid('moreinfo').scrollTo(0,0);}

	updatenavbar();
}
