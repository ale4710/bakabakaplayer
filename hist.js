var radiohistory = [], historycleartm = 'blank', songchanged = true, pllexists = false, historyScrollTimeout = 0, historyNoScroll = true;

function historytoggle() {
    if(currentpage === 2) {
        currentpage = 0;
        eid('main').style.display = '';
        eid('history').style.display = 'none';
        /* eid('statusbar-color').content = '#000000'; */
        //historycleartm = setTimeout(function(){eid('historyin').innerHTML = ''},30000);
    } else {
        historygrab();
        currentpage = 2;
        eid('main').style.display = 'none';
        eid('history').style.display = '';
        /* eid('statusbar-color').content = '#252525'; */
    }

    updatenavbar();
}

function historyK(k) {
    if(
        keyisnav(k) || 
        k.key === 'Backspace'
    ){k.preventDefault();}

    switch(k.key) {
        case 'Backspace':
            historytoggle();
            break;
        case 'ArrowDown':
            historyScrollReset();
            historyitemsel = navigatelist(historyitemsel,document.getElementsByClassName('historyitem'),1);
            scrolliv(document.activeElement,true);
            
            break;
        case 'ArrowUp':
            historyScrollReset();
            historyitemsel = navigatelist(historyitemsel,document.getElementsByClassName('historyitem'),-1);
            scrolliv(document.activeElement,false);
            break;
        case 'SoftLeft':
            historygrab();
            break;
        case 'SoftRight':
            sharemdata(radiohistory[historyitemsel].title,radiohistory[historyitemsel].artist,radiohistory[historyitemsel].album,radiohistory[historyitemsel].albumid);
            break;
        case 'Enter':
            openalbum(radiohistory[historyitemsel].albumid);
            break;
    
    }
}

function historygrab() {
    //cancelAnimationFrame(historyScrollWatcher); //cancel the frame, don't wanna have two of these running at the same time.
    historyNoScroll = true; //disable scrolling
    if(serverpingreq) {
        eid('historyin').innerHTML = '<center><img src="/img/brdc.gif" /><br>Waiting for previous request...</center>';
        if(!pllexists) {
            serverpingr.addEventListener('ploaded',historygrab);
            pllexists = true;
        }
    } else {
        //don't need to check because removing non existant event listener is ok
        serverpingr.removeEventListener('ploaded',historygrab);
        pllexists = false;

        eid('historyin').innerHTML = '<center><img src="/img/brdc.gif" /><br>Loading...</center>';
    }
    metadatagrab(1);
    historyitemsel = 0;
}

function historygrabdone() {
    if(serverpingr.readyState === 4) {
        eid('historyin').innerHTML = '';
        var resp = JSON.parse(serverpingr.response);
        radiohistory = [];
        for(var i=0;i<resp.length;i++) {
            var respsh = new Object;
            respsh.title = resp[i].TITLE;
            respsh.artist = resp[i].ARTIST;
            respsh.album = resp[i].ALBUM;
            respsh.albumid = resp[i].ALBUMID;

            var pushit = true;
            if(radiohistory.length === 0) {
                radiohistory.push(respsh);
            } else {
                var prevresp = radiohistory[radiohistory.length - 1];
                //if any of the fields are different, for some reason we get duplicates sometimes when pulling data from server
                if(prevresp.title !== respsh.title || prevresp.artist !== respsh.artist || prevresp.album !== respsh.album || prevresp.albumid !== respsh.albumid) { //do else if because of evil undefined shit
                    radiohistory.push(respsh); 
                } else {
                    pushit = false;
                }
            }

            if(pushit) {
                var goout = document.createElement('div');
                goout.setAttribute('tabindex','-1');
                goout.setAttribute('class','historyitem');
                var art = 'https://gensokyoradio.net/images/albums/500/' + resp[i].ALBUMART;
                if(resp[i].ALBUMART === '' || !prefm('read','imgload')) {art = '/img/noalbum_line.png';}

                /* endapnd = '<div class="historyitem_npc"><div class="historyitem_np"><img src="/img/currentlyplaying.png" /><br>CURRENT<br>SONG</div></div>'; */

                goout.innerHTML = '<div class="historyitem_imgc"><img src="' + art + '" class="historyitem_img fitCenterContainer" /></div><div class="historyitem_textcnt"><div><span>' + resp[i].TITLE + '</span></div><div class="historyitem_textcnt_artist"><span>' + resp[i].ARTIST + '</span></div><div class="historyitem_textcnt_time"><span>' + timedateformat(((new Date().getTime()) / 1000) - resp[i].PLAYED) + '</span></div></div>';
                eid('historyin').appendChild(goout);
            }
        }
        document.getElementsByClassName('historyitem')[0].focus();
        historyScrollTimeout = 0;
        historyNoScroll = false;
        historyScrollWatcher();
        setTimeout(metadatagrabfinalize,5000);
    }
}

function historygraberror(e) {
    console.log(e);
    setTimeout(metadatagrabfinalize,5000);
}

function historyScrollWatcher() {
    if(currentpage !== 2 || historyNoScroll) {return}
    requestAnimationFrame(historyScrollWatcher);

    if(historyScrollTimeout < 30) {
        historyScrollTimeout++;
    } else {
        var spaceavail = screen.availWidth - 52;
        for(var i = 0; i < 2; i++) {
            var curel = document.activeElement.getElementsByClassName('historyitem_textcnt')[0].children[i].children[0];
            if(
                curel.offsetWidth > spaceavail && 
                curel.offsetLeft > spaceavail - curel.offsetWidth - 2
            ) {
                curel.style.left = (Number(curel.style.left.substring(0,curel.style.left.length - 2)) - 1.5) + 'px';
            }
        }
    }
}
function historyScrollReset() {
    historyScrollTimeout = 0;
    for(var i = 0; i < 2; i++) {
        document.activeElement.getElementsByClassName('historyitem_textcnt')[0].children[i].children[0].style.left = 0;
    }
}
