if(currentquality < 1) {
    currentquality = 1;
    localStorage.setItem('lastquality','1');
    audio.src = 'https://stream.gensokyoradio.net/1/';
}

window.addEventListener('load',function(){
    qualitydispupdate();

    if(prefm('read','compact')) {
        document.body.classList.add('compact');
    } 
    
    eid('mdataimg').addEventListener('load',function(){
        eid('mdataimgoverlay').style.opacity = null;
    });
    eid('mdataimg').addEventListener('error',function(){
        alertMessage('Album art failed to load this time...',2500,2);
        eid('mdataimgoverlay').style.opacity = null;
    });
    
    refreshinterval = setInterval(checkmetadatagrab,1000);
    
    prefm('redraw');

});






function prefm(actionread,setting,value,inmenu) {
    switch(actionread) {
        case 'write':
            if(typeof(setting) === 'string') {
                var valueout = value;
                if(typeof(valueout) === 'object') {
                    valueout = JSON.parse(valueout);
                }
                if(typeof(value) === 'boolean' && inmenu) {
                    document.getElementsByClassName('optionpref')[lookfor(document.getElementsByClassName('optionoptc-pref'),document.getElementsByClassName('optionoptc')[optselectKindex])].style.opacity = Number(value);
                }
                localStorage.setItem(setting,valueout);
                return value;
            } else {
                console.error('prefm access error: setting to call must be a string.');
                return;
            }
        case 'read':
            if(typeof(setting) === 'string') {
                var toret = localStorage.getItem(setting);
                switch(toret) {
                    case 'true':
                        return true;
                    case 'false':
                        return false;
                    default:
                        return toret;
                }
            } else {
                console.error('prefm access error: setting to call must be a string.');
                return;
            }
        case 'redraw':
            if(localStorage.getItem('firstrun') === null) {
                localStorage.setItem('firstrun','1');
                localStorage.setItem('imgload','true');
                localStorage.setItem('bgload','false');
                localStorage.setItem('compact','false');
                break;
            }
            var avst = ['imgload','bgload','compact'];
            for(i=0;i<document.getElementsByClassName('optionoptc').length - brtoptions - prefopts;i++) {
                var st = localStorage.getItem(avst[i]);
                if(st === 'false') {
                    document.getElementsByClassName('optionpref')[i].style.opacity = null;
                } else if(st === 'true') {
                    document.getElementsByClassName('optionpref')[i].style.opacity = 1;
                } //else skip it
            }
            break;

    }
}

