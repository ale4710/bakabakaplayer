var navbar = {
    "myself": eid('navbar'),
    "left": eid('navbar-left'),
    "right": eid('navbar-right'),
    "center": eid('navbar-center')
};

function eid(d) {return document.getElementById(d);}

function timeformat(input) {
    var m = Math.floor(input / 60), s = Math.floor(input % 60);
    if(m < 10) {
        m = '0' + m;
    }
    if(s < 10) {
        s = '0' + s;
    }
    return m + ':' + s;
}

function dateformat(input) { //expecting unix time seconds only; no milis. returns a string with year, month, day, hour, minute, and second
    var dateinit = new Date(input * 1000);
    var times = [
        dateinit.getMonth() + 1,
        dateinit.getDate(),
        dateinit.getHours(),
        dateinit.getMinutes(),
        dateinit.getSeconds()
    ];
    for(i=0;i<times.length;i++) {
        if(times[i] < 10) {times[i] = '0' + times[i];}
    }
    return dateinit.getFullYear() + '-' + times[0] + '-' + times[1] + ' ' + times[2] + ':' + times[3] + ':' + times[4];
}

function timedateformat(input) { //expecting unix time seconds only; no milis. returns a string with year, month, day, hour, minute, and second
    var dateinit = new Date(input * 1000);
    var times = [
        dateinit.getHours(),
        dateinit.getMinutes(),
        dateinit.getSeconds()
    ];
    for(i=0;i<times.length;i++) {
        if(times[i] < 10) {times[i] = '0' + times[i];}
    }
    return times[0] + ':' + times[1] + ':' + times[2];
}


function sharemdata(title,artist,album,albumid,returnpage) {
    var toshare = '「' + title + '」 by 「' + artist + '」 from 「' + album + '」';
    if(albumid) {toshare += '\n' + createAlbumURL(albumid);}
    shareot(toshare,returnpage);
}

function openalbum(number) {
    openlink(createAlbumURL(number));
}
function createAlbumURL(num) {
    return 'https://gensokyoradio.net/music/album/' + num + '/';
}

function openlink(link) {
    if(window.confirm('Open this link in the browser?\n' + link)) {
        var abl = window.open(link,'_blank');
        abl.focus();
    }
}

function navigatelist(index,list,move) { //RETURNS the current index of the thing.
    if(move > 0) { //going down
        if(index + 1 < list.length) {
            index++;
        } else {
            index = 0;
        }
    } else if(move < 0) {
        if(index - 1 > -1) {
            index--;
        } else {
            index = list.length - 1;
        }
    }

    list[index].focus();
    return index;
}

function canseesc(cn,el) { //cn container, el,,, element
    var 
    ct = cn.scrollTop,
    cb = ct + cn.clientHeight,
    et = el.offsetTop,
    eb = et + el.clientHeight;
    //IMPORTANT NOTE: IF USING THIS FUNCTION PLEASE MAKE SURE THAT THE PARENT IS POSITIONED
    //more info https://developer.mozilla.org/en-US/docs/Web/API/HTMLelement/offsetParent

    return ((eb <= cb) && (et >= ct)); //adapted from https://stackoverflow.com/a/488073
}

function scrolliv(el,dn,elpr) {
    if(elpr === undefined) {
        elpr = el.parentElement;
    }
    if(!canseesc(elpr,el)) {
        if(dn) { //going down
            el.scrollIntoView(false); //align to bottom
        } else { //going up
            el.scrollIntoView(true); //align to top
        }
    }
}

function radiobtnsel(list,sel) {
    for(var i = 0; i < list.length; i++) {
        list[i].style.display = 'none';
    }
    list[sel].style.display = null;
}

function lookfor(list,obj) {
    for(var i = 0; i < list.length; i++) {
        if(list[i] === obj) {
            return i;
        }
    }
}

function updatenavbar() {
    switch(currentpage) {
        case 0:
            navbar.left.textContent = 'More Info';
            navbar.right.textContent = 'menu';
            if(audio.paused) {
                navbar.center.textContent = 'play';
            } else {
                navbar.center.textContent = 'stop';
            }
            break;
        case 1:
            navbar.left.textContent = '';
            navbar.right.textContent = 'back';
            navbar.center.textContent = 'select';
            break;
        case 2:
            navbar.left.textContent = 'refresh';
            navbar.right.textContent = 'share';
            navbar.center.textContent = 'open';
            break;
        case 3:
            navbar.left.textContent = 'Less Info';
            navbar.right.textContent = '';
            if(navbaropendispcheck()) {
                navbar.center.textContent = 'open';
            } else {
                navbar.center.textContent = '';
            }
            break;

        case 4:
            navbar.left.textContent = '';
            if(aboutlinksm) {
                navbar.right.textContent = 'scroll';
                navbar.center.textContent = 'open';
            } else {
                navbar.right.textContent = 'select';
                navbar.center.textContent = '';
            }
            break;
        case 5:
            navbar.left.textContent = 'back';
            navbar.right.textContent = '';
            navbar.center.textContent = 'select';
            break;
    }
}
function updatefocus() { //this should be used when switching pages.
    switch(currentpage) {
        case 1: document.getElementsByClassName('optionoptc')[optselectKindex].focus(); break;
        case 2: document.getElementsByClassName('historyitem')[historyitemsel].focus(); historyScrollWatcher(); break;
    }
}

var alertMessageTO = false;
function alertMessage(message,disptime,type,image) {
    if(alertMessageTO !== false) {
        clearTimeout(alertMessageTO);
    }
    alertMessageTO = setTimeout(function(){
        alertMessageTO = false;
        eid('alertMessage').classList.add('hidden');
    },disptime);

    switch(type) {
        default:
            eid('alertMessage').style.background = null;
            eid('alertMessage').style.color = null;
            break;
        case 0:
            eid('alertMessage').style.background = '#000';
            eid('alertMessage').style.color = null;
            break;
        case 1:
            eid('alertMessage').style.background = '#fff';
            eid('alertMessage').style.color = '#000';
            break;
        case 2:
            eid('alertMessage').style.background = '#ff0';
            eid('alertMessage').style.color = '#000';
            break;
        case 3:
            eid('alertMessage').style.background = '#f00';
            eid('alertMessage').style.color = null;
            break;
    }
    eid('alertMessageText').textContent = message; //innertext allows linebreaks
    if(image) {
        eid('alertMessage').classList.remove('noimage');
        eid('alertMessageImg').src = image;
    } else {
        eid('alertMessage').classList.add('noimage');
    }

    eid('alertMessage').classList.remove('hidden');
}


if('mozAudioChannelManage' in navigator) {
    navigator.mozAudioChannelManager.addEventListener('headphoneschange',function(){
        if(!navigator.mozAudioChannelManager.headphones) {
            audio.pause();
        }
    }); //headhpone handeler,eg if unplug
}




/* slightly unimportant */

function keyisnav(k) { return ['ArrowDown','ArrowUp','ArrowLeft','ArrowRight'].indexOf(k.key) > -1; }

function qualitydispupdate() {
    switch(currentquality) {
        case 1:
            eid('mdata-various-bitrate').textContent = '128kbps';
            break;
        case 2:
            eid('mdata-various-bitrate').textContent = '64kbps';
            break;
        case 3:
            eid('mdata-various-bitrate').textContent = '256kbps';
            break;
    }
}