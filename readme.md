# baka⑨player

a gensokyo radio player for kaios.

it is named after a certain ice themed character - ~~letty whiterock~~ cirno!

this repository along with many others on my profile was created so that people can try out my applications before it is stable.

## download stable version
website: https://alego.web.fc2.com/kaiosapps/bakabakaplayer/  
bh store: https://store.bananahackers.net/#b9p

## attribution
Tick icon is by FreePik:  
https://www.flaticon.com/free-icon/tick_446191/

Headphones icon is by Becris:  
https://www.flaticon.com/free-icon/headphones_876813/
