var thingToShare, sharelastpage;
function shareot(input,returnpage) {
	thingToShare = input;

	if(returnpage === null || returnpage === undefined) {returnpage = currentpage} 
	//doing the above instead of
	//	sharelastpage = currentpage || returnpage
	//because 0 is false in javascript's logic
	sharelastpage = returnpage;

	currentpage = 5;
	document.getElementsByClassName('shareot-opt')[0].focus();
	eid('shareot-cont').style.opacity = 1;
	updatenavbar();

}

function shareCMA(method) { //share Call MozActivity
	method = [
		'email',
		'sms',
		'whatsapp',
		'twitter'
	][method];
	var mza;
	switch(method) {
		case 'email':
			mza = new MozActivity({
				name: 'new',
				data: {
					type: 'mail',
					URI: 'mailto:?body=' + encodeURIComponent(thingToShare), //for email
					//body: input //for text messaging
			}});
			break;
		case 'sms':
			mza = new MozActivity({
				name: 'new',
				data: {
					type: 'websms/sms',
					//URI: 'mailto:?body=' + encodeURIComponent(input), //for email
					body: thingToShare //for text messaging
			}});
			break;
		case 'whatsapp':
			mza = new MozActivity({
				name: 'view',
				data: {
					type: 'url',
					url: 'https://wa.me/?text=' + encodeURIComponent(thingToShare)
			}});
			break;
		case 'twitter':
			mza = new MozActivity({
				name: 'view',
				data: {
					type: 'url',
					url: 'http://twitter.com/share?text=' + encodeURIComponent(thingToShare)
			}});
			break;
	}

	mza.onerror = function(e){
		console.log(e);
		switch(e.target.error.name) {
			case 'NO_PROVIDER': alertMessage('App not found.',5000,0); break;
			case 'ActivityCanceled': alertMessage('Share canceled.',5000,0); shareotReturn(); break;
			default: alertMessage('Share failed.'); break;
		}
	}
	mza.onsuccess = shareotReturn;
}


function shareotK(k) {
	switch(k.key) {
		case 'Backspace':
		case 'SoftLeft':
			shareotReturn();
			k.preventDefault();
			break;
		case 'ArrowUp': navigatelist(Number(document.activeElement.tabIndex),document.getElementsByClassName('shareot-opt'),-1); break;
		case 'ArrowDown': navigatelist(Number(document.activeElement.tabIndex),document.getElementsByClassName('shareot-opt'),1); break;
		case 'Enter': shareCMA(Number(document.activeElement.tabIndex));
	}
}
function shareotReturn() {
	eid('shareot-cont').style.opacity = 0;
	currentpage = sharelastpage;
	updatenavbar();
	updatefocus();
}