var 
currentquality = Number(localStorage.getItem('lastquality')),
audionothing = URL.createObjectURL(new Blob([], {type:"audio/mp3"})),
audio = new Audio(), 
currentpage = 0,
audiobaseurl = 'https://stream.gensokyoradio.net/';

window.addEventListener('DOMContentLoaded',()=>{
    audio.mozAudioChannelType = 'content'; //audio bg play
    audio.preload = 'none';
    navigator.mozAudioChannelManager.volumeControlChannel = 'content';
});


//debug
/* window.addEventListener('keyup',function(k){
    eid('debugoutput').innerHTML += '<br>' + k.key + ' up';
}); */

window.addEventListener('keydown', keyHandle); //key handelr
function keyHandle(k) {
    
    //debug
    //eid('debugoutput').innerHTML += '<br>' + k.key;

    //switch for absolutely global stuff. mainly headphone events.

    /* bluetooth control and headphone stuff

        MediaPlay
        MediaPause
        MediaTrackNext
        MediaTrackPrevious

        HeadsetHook

    */

    switch(k.key) {
        case "MediaPlay":
        case "MediaPause":
        case "HeadsetHook":
            audiopausetoggle();
            break;
    }

    switch(currentpage) {
        case 1: optionK(k); return;
        case 2: historyK(k); return;
        case 3: moreinfoK(k); return;
        case 4: aboutK(k); return;
        case 5: shareotK(k); return;
        
    }

    if(keyisnav(k)) {
        k.preventDefault();
    }

    switch(k.key) {
        case "Enter":
            audiopausetoggle();
            break;
        case "SoftRight":
            optionK();
            break;
        case "SoftLeft":
            moreinfoK();
            break;
        case "ArrowUp":
            navigator.volumeManager.requestUp();
            break;
        case 'ArrowDown':
            navigator.volumeManager.requestDown();
            break;
        //debug stuff
        //case '3':
        //    window.open(eid('mdataimg').src);
        //    break;
        //case '1':
        //    window.alert(serverpingr.response);
        //    break;
    }

    updatenavbar();
}

//// MENU ////
var optselectKindex = 0, prefopts = document.getElementsByClassName('optionpref').length, brtoptions = document.getElementsByClassName('optionopt').length;
function optionK(k) {
    var qlist = document.getElementsByClassName('optionoptc');
    if(currentpage !== 1) {
        currentpage = 1;
        eid('option').style.opacity = 1;
        optselectKindex = 0;
        qlist[optselectKindex].focus();
        radiobtnsel(document.getElementsByClassName('optionradio'),currentquality - 1);
        return "Page 1";
    } else {

        if(
            keyisnav(k) || 
            k.key === 'Backspace'
        ){k.preventDefault();}

        switch(k.key) {
            case "ArrowDown":
                optselectKindex = navigatelist(optselectKindex,qlist,1);
                break;
            case "ArrowUp":
                optselectKindex = navigatelist(optselectKindex,qlist,-1);
                break;
            case "Enter":
                if(qlist[optselectKindex].classList.contains('optionoptc-pref')) { //if is setting

                    switch(lookfor(document.getElementsByClassName('optionoptc-pref'),qlist[optselectKindex])) {
                        case 0:
                            prefm('write','imgload',!prefm('read','imgload'),true);
                            break;
                        case 1:
                            prefm('write','bgload',!prefm('read','bgload'),true);
                            break;
                        case 2:
                            prefm('write','compact',!prefm('read','compact'),true);
                            if(eid('moreinfo').style.opacity === '1') {break;}
                            if(prefm('read','compact') ) {
                                document.body.classList.add('compact');
                            } else {
                                document.body.classList.remove('compact');
                            }
                            break;
                    }
                } else if(qlist[optselectKindex].classList.contains('optionoptc-bitr')) { //if is bitrate change

                    var changebitrat = lookfor(document.getElementsByClassName('optionoptc-bitr'),qlist[optselectKindex]);
                    radiobtnsel(document.getElementsByClassName('optionradio'),changebitrat);
                    currentquality = changebitrat + 1;
                    localStorage.setItem('lastquality',currentquality);
                    qualitydispupdate();
                    var playing = !audio.paused;
                    audio.src = audiobaseurl + currentquality;
                    if(playing) {
                        audio.play();
                    }
                } else { //finally,,,

                    switch(optselectKindex) {
                        case 0: //share
                            if(currentsongdata.advert) {
                                alertMessage('This isn\'t a song...',2500,0);
                                break;
                            }
                            eid('option').style.opacity = 0;
                            if(currentsongdata.live) {
                                shareot('Gensokyo Radio is live!\n' + eid('mdata-title').textContent + '\nhttps://gensokyoradio.net/music/playing/',0);
                            } else {
                                sharemdata(
                                    eid('mdata-title').textContent,
                                    eid('mdata-artist').textContent,
                                    eid('mdata-album').textContent,
                                    currentsongdata.albumid,
                                    0);
                            }
                            break;
                        case 1: //history
                            eid('option').style.opacity = 0;
                            historytoggle();
                            break;
                        case 2: //refresh
                            if(serverpingreq) {
                                alertMessage('Refresh request not sent; wait for a little.',2500,2);
                            } else {
                                alertMessage('Refreshing...',2500,0);
                                metadatagrab(0);
                            }
                            break;
                        case 3:
                            eid('option').style.opacity = 0;
                            showabout();
                            break;
                        case 4:
                            openlink('https://alego.web.fc2.com/kaiosapps/bakabakaplayer/q.html');
                            break;
                        default:
                            console.log('main menu: what the heck?');
                            break;
                    }
                }
                break;
            case "SoftRight":
            case "Backspace":
                currentpage = 0;
                eid('option').style.opacity = 0;
                document.activeElement.blur();
                updatenavbar();
                break;
        }
    }
}

function audiopausetoggle() {
    if(audio.paused) {
        audio.src = audiobaseurl + currentquality;
        audio.play();
    } else {
        audio.pause();
    }
}
/* audio update events etc */
audio.addEventListener('play',function(){
    audioontg();
});
audio.addEventListener('pause',function(){
    audio.src = audionothing;
    audioontg();
});
audio.addEventListener('error',function(e){
    leftsongstatusupdate('error');
    console.log(e);
});
audio.addEventListener('stalled',function(){leftsongstatusupdate('stall')});
audio.addEventListener('playing',function(){leftsongstatusupdate('playing')});
audio.addEventListener('waiting',function(){leftsongstatusupdate('waiting')});

function audioontg() {
    leftsongstatusupdate();
    updatenavbar();
}
function leftsongstatusupdate(type) {
    if(type) {
        switch(type) {
            case "stall":
                eid('mdata-various-leftetcinfo').textContent = 'Buffering...';
                break;
            case "error":
                alertMessage('An audio error occured.',5000,3);
                eid('mdata-various-leftetcinfo').textContent = 'Error occured.';
                break;
            case "playing":
                eid('mdata-various-leftetcinfo').textContent = 'Playing.';
                break;
            case "waiting":
                eid('mdata-various-leftetcinfo').textContent = 'Waiting...';
                break;
            case "mdataerr":
                alertMessage('A error occured while trying to load metadata.',5000,2);
                eid('mdata-various-leftetcinfo').textContent = 'Metadata error.';
            }

    } else {

        if(audio.paused) {
            eid('mdata-various-leftetcinfo').textContent = 'Stopped.';
        } else {
            eid('mdata-various-leftetcinfo').textContent = 'Playing.';
        }
    }
}