function showabout() {
	if(currentpage === 4) {
        currentpage = 0;
        eid('main').style.display = '';
		eid('about').style.display = 'none';
		aboutlinksm = false;
	} else {
		currentpage = 4;
        eid('main').style.display = 'none';
		eid('about').style.display = '';
		eid('aboutin').scrollTo(0,0);
		eid('aboutin').focus();
		loadadvert();

	}

	updatenavbar();
}

var aboutlinksel = 0, aboutlinksm = false;
function aboutK(k) {
	switch(k.key) {
		case 'Backspace':
			k.preventDefault();
			showabout(); //this should toggle it back...
			break;

		case 'ArrowUp':
			if(aboutlinksm) {
				k.preventDefault();
				aboutlinksel = navigatelist(
					aboutlinksel,
					eid('aboutin').getElementsByClassName('link'),
					-1
				);
			}
			break;

		case 'ArrowDown':
			if(aboutlinksm) {
				k.preventDefault();
				aboutlinksel = navigatelist(
					aboutlinksel,
					eid('aboutin').getElementsByClassName('link'),
					1
				);
			}
			break;

		case 'SoftRight':
			aboutlinksm = !aboutlinksm;
			if(aboutlinksm) {
				eid('aboutin').getElementsByClassName('link')[aboutlinksel].focus();
			} else {
				eid('aboutin').getElementsByClassName('link')[aboutlinksel].blur();
			}

			updatenavbar();
			break;

		case 'Enter':
			if(aboutlinksm) {
				openlink(eid('aboutin').getElementsByClassName('link')[aboutlinksel].dataset.href);
			}
			break;


	}
}